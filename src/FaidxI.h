// -*-mode:c++; c-style:k&r; c-basic-offset:4;-*-
//
// Copyright 2023, Julian Catchen <jcatchen@illinois.edu>
//
// This file is part of Stacks.
//
// Stacks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Stacks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Stacks.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __FAIDXI_H__
#define __FAIDXI_H__

#include "input.h"

class Faidx: public Input {
    map<string, long> index;
    string buf;

public:
    Faidx(const char *path) : Input(path) {
        ifstream idx_fh;
        string   index_path = string(path) + ".fai";
        
        //
        // Open and parse the index file for the FASTA.
        //
        idx_fh.open(index_path, ifstream::in);

        if (idx_fh.fail()) {
            cerr << "Error opening FASTA index file: '" << index_path << "'\n";
            throw exception();
        }
        
        vector<string> parts;
        long line = 1;
        do {
            idx_fh.getline(this->line, max_len);

            if (idx_fh.eof())
                break;
            
            parse_tsv(this->line, parts);
            if (parts.size() != 5) {
                cerr << "Unable to parse FASTA index file, error on line " << line << "\n";
                throw exception();
            }
            this->index[parts[0]] = is_integer(parts[2].c_str());
            line++;
        } while (idx_fh.good());
        
        cerr << "Parsed '" << index_path << "', found " << this->index.size() << " chromosomes.\n";
        idx_fh.close();
        
        //
        // Open the input FASTA file.
        //
        if (this->fh.fail())
            cerr << "Error opening input file '" << path << "'\n";
        if (fh.peek() != '>') {
            cerr << "Error: '" << path << "': not in fasta format (expected '>').\n";
            throw exception();
        }
    };

    Faidx(string path) : Faidx(path.c_str()) { };
    ~Faidx() {};
    long offset(string chr) { return this->index.count(chr) > 0 ? this->index[chr] : -1; }
    Seq *next_seq();
    int  next_seq(Seq &);
    int  read_seq(string, Seq &);
};

inline
Seq *Faidx::next_seq()
{
    return NULL;
}

inline
int Faidx::read_seq(string name, Seq &s)
{
    this->buf.clear();

    //
    // Jump to the proper offset in the file.
    //
    long offs = this->index[name];
    if (offs < 0) {
        cerr << "Unable to find entry for '" + name + "' in FASTA index list.\n";
        throw exception();
    }
    this->fh.seekg(offs, this->fh.beg);
        
    //
    // Read the sequence from the file -- keep reading lines until we reach the next
    // record or the end of file.
    //
    this->fh.getline(this->line, max_len);

    size_t len;
    while (this->line[0] != '>' && this->fh.good()) {
        len = strlen(this->line);
        if (len > 0 && this->line[len - 1] == '\r') this->line[len - 1] = '\0';

        this->buf    += this->line;
        this->line[0] = '\0';
        this->fh.getline(this->line, max_len);
    }

    if (this->fh.eof()) {
        len = strlen(this->line);
        if (len > 0 && this->line[len - 1] == '\r') this->line[len - 1] = '\0';

        this->buf += this->line;
    }

    s.reserve(buf.length(), false);
    strcpy(s.seq, this->buf.c_str());

    return 1;
}

inline
int Faidx::next_seq(Seq &s)
{
    this->buf.clear();

    //
    // Check the contents of the line buffer. When we finish reading a FASTA record
    // the buffer will either contain whitespace or the header of the next FASTA
    // record.
    //
    while (this->line[0] != '>' && this->fh.good() ) {
        this->fh.getline(this->line, max_len);
    }

    if (!this->fh.good()) {
        return 0;
    }

    //
    // Check if there is a carraige return in the buffer
    //
    uint len = strlen(this->line);
    if (this->line[len - 1] == '\r') this->line[len - 1] = '\0';

    //
    // Check if the ID line of the FASTA file has a comment after the ID.
    //
    char* q = this->line + 1;
    ++q;
    while (*q != '\0' && *q != ' ' && *q != '\t')
        ++q;
    if (*q != '\0') {
        // Comment present.
        *q = '\0';
        ++q;
        s.comment.assign(q);
    }
    assert(s.id != NULL);
    strcpy(s.id, this->line + 1);

    //
    // Read the sequence from the file -- keep reading lines until we reach the next
    // record or the end of file.
    //
    this->fh.getline(this->line, max_len);

    while (this->line[0] != '>' && this->fh.good()) {
        len = strlen(this->line);
        if (len > 0 && this->line[len - 1] == '\r') this->line[len - 1] = '\0';

        this->buf    += this->line;
        this->line[0] = '\0';
        this->fh.getline(this->line, max_len);
    }

    if (this->fh.eof()) {
        len = strlen(this->line);
        if (len > 0 && this->line[len - 1] == '\r') this->line[len - 1] = '\0';

        this->buf += this->line;
    }

    s.reserve(buf.length(), false);
    strcpy(s.seq, this->buf.c_str());

    return 1;
}

#endif // __FAIDXI_H__
