Source: stacks
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               automake,
               zlib1g-dev,
               libbam-dev,
               libsparsehash-dev,
               samtools,
               libhts-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/stacks
Vcs-Git: https://salsa.debian.org/med-team/stacks.git
Homepage: https://creskolab.uoregon.edu/stacks/
Rules-Requires-Root: no

Package: stacks
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         samtools,
         libdbi-perl,
         bsdextrautils,
         python3
Recommends: libspreadsheet-writeexcel-perl,
            littler
Suggests: gdb
Provides: stacks-web
Description: pipeline for building loci from short-read DNA sequences
 Stacks is a software pipeline for building loci from short-read sequences,
 such as those generated on the Illumina platform. Stacks was developed to work
 with restriction enzyme-based data, such as RAD-seq, for the purpose of
 building genetic maps and conducting population genomics and phylogeography.
 .
 Note that this package installs Stacks such that all commands must be run as:
 $ stacks <cmdname> <args>
